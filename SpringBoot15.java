import java.time.Duration;
import java.time.LocalDateTime;

public class SpringBootProblem15 {
    public Duration calculateDuration(String startDateStr, String endDateStr) {
        LocalDateTime startDate = new LocalDateTime(startDateStr);
        LocalDateTime endDate = new LocalDateTime(endDateStr);
        return Duration.between(startDate, endDate);
    }
}
