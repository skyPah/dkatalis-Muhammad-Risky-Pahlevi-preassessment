public class SpringBootProblem16 {
    public int sumOfPrimes(int amount) {
        int total = 0;
        int countPrimes = 0;
        int currentNumber = 0;
        while (countPrimes < amount) {
            if (( currentNumber%2 != 0 && currentNumber%3 != 0 &&
            currentNumber%5 != 0 && currentNumber%7 != 0 ) || currentNumber==2 ||
            currentNumber==3 || currentNumber==5 || currentNumber==7) {
                total = total + currentNumber;
                countPrimes = countPrimes + 1;
            }
            currentNumber = currentNumber + 1;
        }
        return total;
    }
}
